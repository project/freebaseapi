How it should work
------------------

From a Clean Drupal 7 (.26) platform
Install as 'minimal'.

Download 
* freebase_api 7.x-1.x
* wslient 7.x-1.x
* http_client 7.x-2.4+
* entity 7.x-1.1+

Enable
* freebase_api
- it should require the rest that are needed

Visit
* Administration » Configuration » Web services » Google Freebase
  /admin/config/services/freebase

Enter some of the sample lookups shown on that page.

Simplytest.me
-------------
at http://simplytest.me/

* Enter wsclient as the main module 
- this is so you can choose the -dev version. Select 7.x-1.x
* Enter 1126680 as an additional project : Freebase API 
* also add the required http_client and entity (Entity API)
* add the patch from https://drupal.org/node/2045847 :
  https://drupal.org/files/issues/2045847-7-optional-args-are-optional.patch

* On startup, login and visit 'modules' and enable 'Freebase API'


Troubleshooting
---------------

If the autocomplete or the lookup return no data.
* See if there is anything in the /admin/reports/dblog

* Try accessing the web service tester instead.
* Enable wsclient_tester
* visit Administration » Configuration » Web services » Web service descriptions
  /admin/config/services/wsclient/manage/freebase
* See if you can get a response or an error message there.

If it's complaining about invalid parameters, you need the patch that makes 
'optional' wsclient parameters actually optional 
(and does not submit empty strings where nothing was expected)
https://drupal.org/comment/8361191#comment-8361191
Or some equivalent.




* download and enable devel.module


