Freebase API
============

Utility to make requests from the Freebase API.

This utility also supports a 'mapping' between CCK+Fields and Freebase
Schemas and attributes. In a similar way to how RDF (should) do it, with
reference to D6/D7 RDF hook_rdf_mapping.

Other modules
-------------------------------------------------------------------------------
This is similar to, and possibly complementary to, but currently different from 
http://drupal.org/project/freebase
The freebase.module project however is dead in 2010.

Recommended
-----------
field_schema_importer
RDFx